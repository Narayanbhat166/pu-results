import requests
import time
from bs4 import BeautifulSoup
from openpyxl import Workbook

wb = Workbook()
ws = wb.active
ws.title = "Final_Result"

url = 'http://karresults.nic.in/resPUC_2020.asp'

regno = 100
cell_no = 2

ws['A1'] = "Name"
ws['B1'] = "Regno"

while True:

    data = {'frmpuc_tokens': '0.3913996',
            'reg': '349'+str(regno)}

    try:
        r = requests.post(url, data=data)
        html_page = BeautifulSoup(r.text, 'html.parser')
    except Exception as e:
        print(e)

    try:

        result = html_page.find(id="result")
        details = html_page.find(id="details")

        tables = html_page.find_all('table')
        td = tables[0].find_all('td')

        Name = td[1].text
        RegNo = td[3].text

        tr = tables[2].find_all('tr')
        td = tr[1].find_all('td')
        Subject1 = td[0].text.strip()

    except Exception as e:
        print(e)
        continue

    try:
        print("Name:"+Name)
        print("RegNo:"+RegNo)

        td = tr[1].find_all('td')
        Subject1Marks = td[3].text.strip()
        Subject1 = td[0].text.strip()

        td = tr[2].find_all('td')
        Subject2Marks = td[3].text.strip()
        Subject2 = td[0].text.strip()

        td = tr[3].find_all('td')
        Subject3Marks = td[3].text.strip()
        Subject3 = td[0].text.strip()

        td = tr[4].find_all('td')
        Subject4Marks = td[3].text.strip()
        Subject4 = td[0].text.strip()

        language = tables[1].find_all('tr')

        td = language[1].find_all('td')
        Language1Marks = td[3].text.strip()
        Language1 = td[0].text.strip()

        td = language[2].find_all('td')
        Language2Marks = td[3].text.strip()
        Language2 = td[0].text.strip()

        ws['A'+str(cell_no)] = Name
        ws['B'+str(cell_no)] = RegNo

        ws['C'+str(cell_no)] = Language1
        ws['D'+str(cell_no)] = Language2
        ws['E'+str(cell_no)] = Subject1
        ws['F'+str(cell_no)] = Subject2
        ws['G'+str(cell_no)] = Subject3
        ws['H'+str(cell_no)] = Subject4

        ws['I'+str(cell_no)] = Language1Marks
        ws['J'+str(cell_no)] = Language2Marks
        ws['K'+str(cell_no)] = Subject1Marks
        ws['L'+str(cell_no)] = Subject2Marks
        ws['M'+str(cell_no)] = Subject3Marks
        ws['N'+str(cell_no)] = Subject4Marks

        result = tables[3].find_all('tr')
        td = result[0].find_all('td')
        marks = td[1].text.strip()

        td = result[1].find_all('td')
        final = td[1].text.strip()

        ws['O'+str(cell_no)] = marks
        ws['P'+str(cell_no)] = final

        print("Final result: "+final)
        print()
        cell_no += 1
    except:
        print("Absent")

    if regno >= 800:
        break

    regno += 1

wb.save("TotalResult.xlsx")
